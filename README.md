This branch contains the code used for the study titled "Exact mapping of Illumina blind spots in the Mycobacterium tuberculosis genome reveals platform-wide and workflow-specific biases.”

# **Software Requirements**

## **OS requirements**
The code and software in this repository was developed, tested, and executed in a Linux (Debian) environment.

## **Software Dependencies**
*  Python version 3.6
*  R version 3.6.1
*  SRA Toolkit 2.9.1
*  Trimmomatic version 0.39
*  Bowtie2 version 2.2.4
*  Samtools version 1.9
*  VarScan version 2.3
*  RAxML version 8.0.0
*  TRF version 4.0.9
*  EMBOSS version 6.6.0

# **Scripts**

## **Sequence Processing**

*  src/sequence_processing/illumina.mk - executes sequence processing pipeline for all genomes
*  src/sequence_processing/illumina-single-isolate.mk - runs software in sequencing processing pipeline for one genome
*  src/sequence_processing/LC_positions.py - takes mpileup file and identifies low coverage positions using relative threshold

## **Phylogenetic Filtering**
*  src/phylogeny/blindspots.py - Performs phylogenetic analysis on blindspots to define true deletions (executes all other scripts in phylogeny directory).
*  src/phylogeny/create_iso_csv_parallel.py - Outputs a csv file that is input for parse_newick.py
*  src/phylogeny/parse_newick.py - Outputs a csv file with the variant of interest, the number of isolates with that variant, and if the isolates make up a monophyetic group on the tree provided
*  src/phylogeny/parse_monophyly_results.py - Identifies monophyletic, paraphyletic, and polyphyletic positions.
*  src/phylogeny/platform_library_combos.py - Outputs position, number of isolates with a position low coverage, and number of isolates in a workflow that have the position (not deleted).  

## **Blind Spot Classification**
* src/classification/classification_pipeline.py - Performs all steps of the blind spot classification process. Imports other scripts in classification directory to perform each step.
* src/classification/sample_isos.py - Randomly selects isolates from a given directory. Outputs a file with lists of isolate names that were selected.
* src/classification/monophyly.py - Contains functions to traverse phylogenetic tree and identify which positions are true deletions.
* src/classification/subset_positions.py - Contains functions for calculating "n" and "G" values using low coverage positions and previously identified true deletions.
* src/classification/EF_combos.py - Contains functions for calculating "E" anf "F" values.
* src/classification/bootstrap_classification.py - Contains functions to classify blind spots using calculated values from equations 3-7.

## **Sequence Attributes**
*  src/seq_attributes/find_homopolymers.sh - Finds homopolymers (length 2+) in the reference genome and outputs their positions to a tsv.
*  src/seq_attributes/gc_content_windows.py - Finds GC content in H37Rv at each position using a set of specified sliding window sizes.
*  src/seq_attributes/bs_gc_window_sizes.R - Takes GC content from each window size and merges them together into one dataframe with blind spot status.
*  src/seq_attributes/homopolymer_threshold.R - Calculates homopolymer length threshold for each iteration
*  src/seq_attributes/repeat_threshold.R - Calculates repeat length threshold for each iteration
*  src/seq_attributes/GC_threshold.R - Calculates GC content threshold for each iteration
*  src/seq_attributes/explained_combos.R - Identifies which positions in the genome meet defined criteria for each attribute.

## **Other Analyses**

*  src/iso_cov_subsets.R - Selects the 25 isolates from each combination that have the most similar coverage distributions to isolates in NextSeq500/TruSeq
*  src/bs_regions.R - Finds consecutive regions of individual blind spots and summarizes their distribution across the genome
*  src/bs_genes.R - Annotates blind spots in coding regions (using pooled set of blind spots)
*  src/workflow-specific-genes.R - Annotates blind spots in coding regions for each workflow (using comparison set) 
*  src/tyler_overlap.R - Finds overlap between blind spots and "ultra-low coverage" hotspots from Tyler et al.
*  src/res-genes-overlap.R - Finds overlap between blind spots and genes implicated in drug resistance
*  src/count_lowcov.py - Counts the number of low coverage positions for each isolate
*  src/coverage_relationship.py - Makes file with isolate, low coverage position, and coverage at that position
*  src/bootstrap_stats.py - Calculates the mean, standard deviation, and variance for each workflow in which isolates were bootstrapped.
*  src/workflow_shared_bs.R - Gets presence/absence of blind spots in each workflow using the "pooled" and the "comparison" sets.
*  src/Figs.R - Generates Most of the figures and executes statistical tests of differences between worklfows


# **Source Code for Figures and Tables**
Figures 1A & 1B were created using the iTOL web tool 
All other figures were created in R using the ggplot2 package.
Code to create figures can be found in the following scripts:
*  Figure 1:
    A. src/seq_attributes/homopolymer_threshold.R  
    B. src/seq_attributes/repeat_threshold.R  
    C. src/Figs.R
*  Figure 2: src/Figs.R
*  Figure 3: src/Figs.R
*  Figure 4:  
    A. src/seq_attributes/homopolymer_threshold.R  
    B. src/seq_attributes/repeat_threshold.R  
    C. src/Figs.R
    D. src/seq_attributes/explained_figures.R  
    E. src/Figs.R
*  Figure 5: src/Figs.R
*  Figure 6: src/Figs.R

Tables:
*  Table 1: src/workflow_shared_bs.R
*  Table 2: src/bs_genes.R and src/workflow-specific genes,R
*  Table 3: src/res-genes-overlap.R
*  Supplemental Table S2: src/classification/classification_pipeline.py
*  Supplemental Table S5: src/classification/classification_pipeline.py
*  Supplemental Table S6: src/bootstrap_stats.py
*  Supplemental Table S7: explained_combos.R
*  Supplemental Table S8: src/seq_attributes/GC_threshold.R
*  Supplemental Table S9: src/bs_genes.R
*  Supplemental Table S10: src/workflow-specific-genes.R

