blindspots_list = '/grp/valafar/data/depot/blindspots/regions/blindspots_list.csv'

# returns list of unique positions
def get_positions(bs_file):
    position_list = []
    with open(bs_file, 'r') as bs:
        for line in bs:
            line = line.strip().split(',')
            position_list.append(line[0])
    position_list = position_list[1:]
    position_set = list(set(position_list))
    return position_set

# writes unique set to a csv

def write_set(position_set):
    with open('/grp/valafar/data/depot/blindspots/regions/blindspots_set.txt', 'w') as csv_open:
        for pos in sorted(position_set, key = lambda x: (len(x), x)):
            csv_open.write(pos + '\n')

position_set = get_positions(blindspots_list)
write_set(position_set)
