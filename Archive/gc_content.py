#!/usr/bin/env python
# Calculate GC content using a sliding window
# The output is two columns (prints to stdout):
# A  position and the GC content for that position
# For H37Rv, window size can be changed by changing
# the 'window' variable

import sys, os, errno
import fileinput
import argparse
import gzip
GROUPHOME = os.environ['GROUPHOME']
h37rv = os.path.join(GROUPHOME, 'resources', 'H37Rv.fasta')
window = 51
pos = 0

with open(h37rv) as f:
    lines="".join(line.strip() for line in f)
    l=len(lines)

for i in range(0,l-window):
    chars=lines[i:i+window]
    GC_count=0
    pos+=1
    for char in chars:
        if char=="G" or char=="C":
            GC_count+=1

    print pos,float(GC_count)/len(chars)

