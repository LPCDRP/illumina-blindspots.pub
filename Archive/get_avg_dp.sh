#!/bin/bash
#
# This shell script loops through all coverage files
# and returns the average depth for each file

for line in /home/tshmaya/Workspace/illumina_blindspots/pacbio_C1/coverage_files/*.coverage.txt
do
  echo "processing " $line
  #avg="$(zcat $line |grep -v @ | grep -v + |awk '{print length}' | awk '{ sum += $1; n++ } END { if (n > 0) print sum / n; }')"
  avg="$(cat $line  | awk '{ sum += $3; n++ } END { if (n > 0) print sum / n; }')"
  echo ${avg}
  #mv /grp/valafar/data/depot/public-genomes/2015-2016-sequences/$line.low_DP/$line.DP_5 /grp/valafar/data/depot/public-genomes/2015-2016-sequences/$line.low_DP/$line.low_DP.DP_5
done 
