#!/usr/bin/env python3

import argparse
import numpy as np

parser = argparse.ArgumentParser(description="""Finds the median size of low coverage positions across the isolates.""")
parser.add_argument("-i", "--input", nargs='+', required=True, help="All low cov position files to be included.")

args = parser.parse_args()
lowcov_files = args.input


def calculate_Klc(lowcov_file):
    Klc = 0
    with open(lowcov_file, 'r') as lowcov_open:
        for line in lowcov_open:
            Klc += 1
    return Klc


def calculate_med_Klc():
    E_list = []
    for SRR in lowcov_files:
        Klc_size = calculate_Klc(SRR)
        # genome_size = 4474555
        # iso_E = float(Klc_size/genome_size)
        # E_list.append(iso_E)
        E_list.append(Klc_size)
    med_Klc = np.median(E_list)
    return med_Klc


if __name__ == '__main__':
    print('Median size of Klc: ' + str(calculate_med_Klc()))
    print('E : ' + str(calculate_med_Klc()/4474555))
