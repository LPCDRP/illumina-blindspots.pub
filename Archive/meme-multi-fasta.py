#!/usr/bin/env python2.7

import argparse

parser = argparse.ArgumentParser(description="""Takes list of blindspots and creates a multi-fasta file. 
                                                Each sequence in the fasta contains the blind spot and 
                                                20bp before and after that position.""")
parser.add_argument("-bs", "--blindspots", required=True, help="List of blind spots")
parser.add_argument("-r", "--reference", required=True, help="Reference fasta to pull context sequences from")
parser.add_argument("-o", "--output", required=False, default='blindspot_context_sequences.fasta',
                    help="Name of output fasta file")
parser.add_argument("-s", "--size", required=False, default='40',
                    help="Size of window. The number of bases that will be in each sequence. For example, "
                         "40 would be 20 bp before and after the blind spot. Must be an even number. ")

args = parser.parse_args()
blindspots = args.blindspots
reference = args.reference
output_name = args.output
size = int(args.size)/2

# get list of positions from the blindspots file
def get_positions(bs_file):
    position_list = []
    with open(bs_file, 'r') as bs:
        for line in bs:
            line = line.strip().split(',')
            position_list.append(line[0])
    position_list = position_list[1:]
    position_set = list(set(position_list))
    return position_set


# go into the reference fasta and extract the base at that position and the context sequence (return a dictionary)
def get_context(positions,ref_file):
    with open(ref_file, 'r') as ref:
        seq_str = ''
        bs_dict = {}
        for line in ref:
            if not line.startswith('>'):
                line = line.strip()
                seq_str += line
    for position in positions:
        header = '>position'+position
        index = int(position) - 1
        if index >= size and index <= (len(seq_str) - size):
            bs_context = seq_str[(index-size):(index+size)]
        elif index < size:
            # first position is negative
            first_pos = (index-size)
            second_pos = (index+size)
            bs_context = seq_str[first_pos:] + seq_str[0:second_pos]
        elif index > (len(seq_str)-size):
            first_pos = (index-size)
            second_pos = (size-(len(seq_str) - index))
            bs_context = seq_str[first_pos:] + seq_str[0:second_pos]
        bs_dict[header] = bs_context
    return bs_dict



# Write the fasta with a header and the sequence
def write_fasta(headerseq_dict):
    with open(output_name, 'w') as fasta_open:
        for rec in sorted(headerseq_dict, key = lambda x: (len(x), x)):
            fasta_open.write(rec + '\n' + headerseq_dict[rec] + '\n')

def main():
    position_set = get_positions(blindspots)
    bs_dict = get_context(position_set, reference)
    write_fasta(bs_dict)


if __name__ == '__main__':
    main()