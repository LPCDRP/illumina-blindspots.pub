#!/bin/bash
#$ -N rm_blind_spots
#$ -pe threaded 12-20
#$ -l h_vmem=125G
#$ -cwd
# The following script filters out
# vcf files to remove blind spots posotions
# Input: platform, path to folder w/ vcf files,
# and a path to output folder for the filtered vcf files
# execute: 
# ./remove_BS_vcf.sh Illumina path/to/vcf path/to/output/dir
# (if the output dir doesn't exist, it will be created)

PLATFORM=${1:-"Illumina"}
VCF_PATH=${2:-"/home/tshmaya/Workspace/illumina_blindspots"}
OUTPUT_FOLDER=${3:-"/home/tshmaya/Workspace/illumina_blindspots/rm_output"}
mkdir -p ${OUTPUT_FOLDER}

LOG_STAMP=-$(date +"%Y%m%d-%H%M%S")

ILLUMINA_BED=/home/tshmaya/Workspace/illumina_blindspots/BED_FILES/Illumina_BS.bed
ION_BED=/home/tshmaya/Workspace/illumina_blindspots/BED_FILES/Ion_Torrent_BS.bed
PACBIOC1_BED=/home/tshmaya/Workspace/illumina_blindspots/BED_FILES/PacBioC1_BS.bed
PACBIOC2_BED=/home/tshmaya/Workspace/illumina_blindspots/BED_FILES/PacBioC2_BS.bed
PACBIOC4_BED=/home/tshmaya/Workspace/illumina_blindspots/BED_FILES/PacBioC4_BS.bed


if [[ ${PLATFORM} == 'Illumina' ]]; then
  BED_FILE=${ILLUMINA_BED}
elif [[ ${PLATFORM} == 'Ion-Torrent' ]]; then
  BED_FILE=${ION_BED}
elif [[ ${PLATFORM} == 'PacBioC1' ]]; then
  BED_FILE=${PACBIOC1_BED}
elif [[ ${PLATFORM} == 'PacBioC2' ]]; then
  BED_FILE=${PACBIOC2_BED}
elif [[ ${PLATFORM} == 'PacBioC4' ]]; then
  BED_FILE=${PACBIOC4_BED}
else
  echo "Platform name must be one of the following five:"
  echo " Illumina, Ion-Torrent, PacBioC1, PacBioC2, PacBioC4 "
  exit
fi

for VCF_FILE in ${VCF_PATH}/*.vcf.gz
do
  FILE_NAME=$(basename ${VCF_FILE})
  ISOLATE="${FILE_NAME%.*}"
  bedtools intersect -v -a ${VCF_FILE} -b ${BED_FILE} -wa -header > ${OUTPUT_FOLDER}/${ISOLATE}
  gzip -f ${OUTPUT_FOLDER}/${ISOLATE}
  ls ${OUTPUT_FOLDER}/${ISOLATE}.gz
done

