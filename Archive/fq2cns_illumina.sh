#!/bin/bash
#
# This shell script will process a list of isolates
# one isolate per line
# and will generate a directory for each isolate and within this dir
# the list of low depth positions will be named isolate.DP_5
# This workflow works for Illumina data

while read line
do
  echo "Processing isolate $line ... "
  extension=".fastq.gz"
  file_1=$line'_1'$extension
  file_2=$line'_2'$extension
  ref="/grp/valafar/resources/H37Rv-NC_000962.3.fasta"
  file_dir='/grp/valafar/data/depot/'
  mkdir /grp/valafar/data/depot/ion_torrent_blindspots/$line
  target_dir='/grp/valafar/data/depot/ion_torrent_blindspots/'$line/
  echo $target_dir

  java -jar $GROUPHOME/bin/trimmomatic-0.36.jar PE -threads 7 -phred33 $target_dir/$file_1 $target_dir/$file_2 -baseout  $target_dir/$line.trimmed_reads.fastq.gz LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15
  
  fq1=$line.trimmed_reads_1P.fastq.gz
  fq2=$line.trimmed_reads_2P.fastq.gz
  
  bwa mem  $ref $target_dir/$fq1  $target_dir/$fq2 -t 8 > $target_dir/$line.bam
   
  samtools sort $target_dir/$line.bam -o $$target_dir/$line.sorted.cram -O CRAM --reference $ref
   
  time samtools mpileup $target_dir/$line.sorted.cram --output-MQ --min-MQ 20 -f $ref -v > $target_dir/$line.vcf
   
  #bcftools call --multiallelic-caller --ploidy 1  --threads 8 $GROUPHOME/data/depot/public-genomes/2015-2016-sequences/$line/$line.vcf  > $GROUPHOME/data/depot/public-genomes/2015-2016-sequences/$line/$line.cns.vcf
   
  zcat $target_dir/$line.vcf  |grep -v '#' | awk '{split($8,a,"DP="); split(a[2],b,";" ); print $2,b[1]; }'   > $target_dir/$line.DP_pos  
  cat $target_dir/$line.DP_pos | awk '{if ($2 < 5 ) print}' > $target_dir/$line/$line.DP_5
  cat /home/share/valafar/data/depot/public-genomes/2015-2016-sequences/$line/$line.DP_pos | awk '{if ($2 > 300 ) print}' > /home/share/valafar/data/depot/public-genomes/2015-2016-sequences/$line/$line.DP_300
   
   #cat /home/share/valafar/data/depot/public-genomes/2015-2016-sequences/$line/$line.cns.vcf |grep -v '#' | awk '{if ($10 ~ /\./) print $2,"0"; else print $2,$6}' > $GROUPHOME/data/depot/public-genomes/2015-2016-sequences/$line/$line.qual_pos
   #cat /home/share/valafar/data/depot/public-genomes/2015-2016-sequences/$line/$line.qual_pos | awk '{if ($2 < 20 ) print}' > /home/share/valafar/data/depot/public-genomes/2015-2016-sequences/$line/$line.qual_20
   #echo "Removing   $GROUPHOME/data/depot/public-genomes/2015-2016-sequences/$line/$line.bam ..."
   #rm  $GROUPHOME/data/depot/public-genomes/2015-2016-sequences/$line/$line.bam 
done < $1

# #remove all samples w/ average depth < 30

#combine all the depths for all positions from all files and count how many times a pos appeared as low depth, if more than 27, print position
#cat /grp/valafar/data/depot/ion_torrent_blindspots/*/*.DP_5 |awk '{print $1}' | sort | uniq -c |awk '{if ($1 > 27 ) print $2}' |sort -g  > count_pos_27


# #cat /grp/valafar/data/depot/illumina_blindspots/*/*/*.DP_5 |awk '{print $1}' | sort | uniq -c |awk '{if ($1 > 27 ) print $2}' |sort -g  > counts_27_DP5
# python combine_depth_positions.py count_pos > new_blindspots_range
# #python combine_depth_positions.py counts_27_DP5  > range_BS_27_DP5
# python test_overlap.py > new_overlap_genes
# # python test_overlap.py range_BS_27_DP5 | awk '{print $2}' |sort -u > uniq_BS_genes_27

# echo There are
# awk '{print $2 }' new_overlap_genes  |sort -u |wc -l
# echo "unique genes in blindspots."
# echo Number of blindspots regions:
# wc -l new_blindspots_range
# echo Number of samples analyzed:
# ls  /home/share/valafar/data/depot/public-genomes/2015-2016-sequences/*/*.DP_5  |wc -l
# echo Number of samples ommited due to low coverage:
# ls  /home/share/valafar/data/depot/public-genomes/2015-2016-sequences/*.low_DP/*.DP_5  |wc -l
# echo ommited due to low coverage
