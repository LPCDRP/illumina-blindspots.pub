#!/bin/bash

while read line 
do
  echo "Aligning sample $line ... "
  file_1=$line/reads_1.fastq.gz
  file_2=$line/reads_2.fastq.gz
  bwa mem  $GROUPHOME/resources/H37Rv_NC000962_3.fasta  $GROUPHOME/data/repo/sra/walker-pmid26116186/$file_1  $GROUPHOME/data/repo/sra/walker-pmid26116186/$file_2 -t 8 > $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line.bam
  samtools sort $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line.bam -o $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line.sorted.cram -O CRAM --reference $GROUPHOME/resources/H37Rv_NC000962_3.fasta
  samtools depth $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line.sorted.cram > $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line.coverage.txt
   cat $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line.coverage.txt | awk  '($3 < 3) ' >  $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line.less_than_3
   python combine_depth_positions.py $line.less_than_3 > $line.low_depth_range
   time samtools mpileup $line.sorted.cram | java -jar /opt/varscan/VarScan.jar pileup2cns - --min-coverage=0 --min-read2=1 --min-avg-qual=1 > $line.varscan.cns
   cat $line.varscan.cns | sort -nk7 > $line.varscan.sorted
    time samtools mpileup $line.sorted.cram --min-MQ 20 --max-depth 100 -f $GROUPHOME/resources/H37Rv_NC000962_3.fasta.bak -v > $line.vcf
    #time samtools mpileup $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line/$line.sorted.cram --max-depth 100 -f $GROUPHOME/resources/H37Rv_NC000962_3.fasta.bak -v > $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line/$line.max_DP_100.vcf
   #samtools view -h -q 20 $line.sorted.cram  > $line.q_morethan20.cram
  time samtools mpileup $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line/$line.sorted.cram -f $GROUPHOME/resources/H37Rv_NC000962_3.fasta.bak -v > $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line/$line.no_filters.vcf
   bcftools call -c $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line/$line.no_filters.vcf  > $GROUPHOME/data/depot/public-genomes/walker-pmid26116186/$line/$line.no_filters.cns.vcf
done < $1
