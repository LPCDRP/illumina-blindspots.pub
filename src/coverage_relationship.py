iso_file = '/grp/valafar/data/depot/blindspots/cass_iso_final.csv'
invariable_bs_file = '/grp/valafar/data/depot/blindspots/invariably-blind.csv'
output_file = '/grp/valafar/data/depot/blindspots/coverage_relationship.csv'

# make a dict with isolate as key and average coverage as value
def avg_cov_iso(isofile):
    average_coverages = {}
    with open(isofile, 'r') as isos:
        for line in isos:
            if not line.startswith('"isolate'):
                line = line.strip().split(',')
                SRR = line[0][1:-1]
                avg_cov = line[2]
                average_coverages[SRR] = avg_cov
    return average_coverages


# get the positions we are looking at into a list
def bs_list(bs_file):
    bs_list = []
    with open(bs_file, 'r') as invariable_bs:
        for line in invariable_bs:
            line = line.strip()
            if line != '.':
                bs_list.append(line)
    return bs_list


def pos_cov(isolate):
    pos_covs = {}
    LC_positions = '/grp/valafar/data/depot/blindspots/cov_files/'+isolate+'.LC_positions.txt'
    with open(LC_positions, 'r') as lc_positions:
        for line in lc_positions:
            line = line.strip().split()
            position = line[0]
            coverage = line[1]
            pos_covs[position] = coverage
    return(pos_covs)


def main():
    file_string = 'isolate,lc_position,coverage\n'
    average_coverages = avg_cov_iso(iso_file)
    for isolate in average_coverages:
        position_coverages=pos_cov(isolate)
        for lc_pos in position_coverages:
            position_string = isolate+','+lc_pos+','+position_coverages[lc_pos]+'\n'
            file_string += position_string
    with open(output_file, 'w') as output:
        output.write(file_string)



if __name__ == '__main__':
    main()
