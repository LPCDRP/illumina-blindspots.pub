import os
import numpy as np
import argparse

parser = argparse.ArgumentParser(description="""Calculates the mean, standard deviation, and variance for number of 
                                        blind spot in for each workflow in which isolates were randomly selected 
                                        (bootstrapped).""")
parser.add_argument("-d", "--directory", required = False,
                    default = '/grp/valafar/data/depot/blindspots/comparisons/bootstraps',
                    help="Root directory in which all subdirectories are located. Default is "
                         "/grp/valafar/data/depot/blindspots/comparisons/bootstraps")
parser.add_argument("-o", "--output", required = False, default='mean-sd-summary.csv',
                    help="Output file name. Default is 'mean-sd-summary.csv'. "
                         "File automatically gets placed in the root directory (see -d).")


args = parser.parse_args()
directory = args.directory
output = args.output


def count_bs(file_name):
    count = 0
    with open(file_name, 'r') as blindspots:
        for line in blindspots:
            count += 1
    return count


def calc_stats(count_list):
    mean = np.mean(count_list)
    stdev = np.std(count_list)
    variance = np.var(count_list)
    return (mean,stdev,variance)


def write_combo_file(dir, mean, stdev, variance):
    with open(dir+'/../mean-sd.tsv', 'w') as stat_file:
        stat_file.write(str(mean) + '\n' + str(stdev) + '\n' + str(variance) + '\n')


def write_all_combos_file(dir, summary):
    file_string = 'combination,isolates,bootstraps,mean.bs,stdev.bs,variance\n'
    for combo_list in summary:
        file_string += (','.join(combo_list)+'\n')
    with open(dir+'/'+output, 'w') as summary_file:
        summary_file.write(file_string)


def main():
    summary = []
    print('Combinations calculated:')
    for subdir, dirs, files in os.walk(directory):
        if subdir.endswith('blindspots'):
            count_list = []
            for file in files:
                count = count_bs(subdir + '/' + file)
                count_list.append(count)
            mean, stdev, var = calc_stats(count_list)
            write_combo_file(subdir, mean, stdev, var)
            subdir_list = subdir.split('/')
            combo = subdir_list[9]
            isos = subdir_list[8].split('_')[0]
            bootstraps = subdir_list[8].split('_')[1]
            summary.append([combo, isos, bootstraps, str(mean), str(stdev), str(var)])
            write_all_combos_file(directory, summary)
            print(subdir[58:-11])
            print('Mean = ' + str(mean))
            print('SD = ' + str(stdev))
            print('Variance = ' + str(var))


if __name__ == '__main__':
    main()