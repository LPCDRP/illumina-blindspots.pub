library(data.table)
library(ggplot2)
library(dplyr)
library(tidyr)

gc_50 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/50window_GC_H37Rv.tsv')
colnames(gc_50) <- c('position', 'GC_content')
gc_100 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/100window_GC_H37Rv.tsv')
colnames(gc_100) <- c('position', 'GC_content')
gc_200 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/200window_GC_H37Rv.tsv')
colnames(gc_200) <- c('position', 'GC_content')
gc_300 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/300window_GC_H37Rv.tsv')
colnames(gc_300) <- c('position', 'GC_content')
gc_400 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/400window_GC_H37Rv.tsv')
colnames(gc_400) <- c('position', 'GC_content')
gc_500 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/500window_GC_H37Rv.tsv')
colnames(gc_500) <- c('position', 'GC_content')
gc_600 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/600window_GC_H37Rv.tsv')
colnames(gc_600) <- c('position', 'GC_content')
gc_700 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/700window_GC_H37Rv.tsv')
colnames(gc_700) <- c('position', 'GC_content')
gc_800 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/800window_GC_H37Rv.tsv')
colnames(gc_800) <- c('position', 'GC_content')
gc_900 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/900window_GC_H37Rv.tsv')
colnames(gc_900) <- c('position', 'GC_content')
gc_1000 <- fread(file = '/grp/valafar/data/depot/blindspots/regions/1000window_GC_H37Rv.tsv')
colnames(gc_1000) <- c('position', 'GC_content')

blindspots <- fread(file = '/grp/valafar/data/depot/blindspots/regions/blindspots_set.csv')
colnames(blindspots) <- 'position'
blindspots$end <- blindspots$position

master = data.table(position = gc_50$position, '50' = gc_50$GC_content, '100' = gc_100$GC_content,
                    '200' = gc_200$GC_content, '300' = gc_300$GC_content, '400' = gc_400$GC_content,
                    '500' = gc_500$GC_content, '600' = gc_600$GC_content, '700' = gc_700$GC_content, 
                    '800' = gc_800$GC_content, '900' = gc_900$GC_content, '1000' = gc_1000$GC_content)

master$blindspot <- ifelse(master$position %in% blindspots$position, 1, 0)
fwrite(master, file = '/grp/valafar/data/depot/blindspots/regions/window-sizes-gc.csv', row.names = FALSE)
