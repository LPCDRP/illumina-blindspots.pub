#!/usr/bin/env python3

import argparse
import gzip

parser = argparse.ArgumentParser(description="""Finds GC content in H37Rv at each position using a set of 
                                                specified window sizes""")
parser.add_argument("-w", "--windows", required = True, help="window sizes separated by commas")
args = parser.parse_args()
windows = args.windows
h37rv = "/grp/valafar/resources/H37Rv.fasta"


def get_sequence(reference):
    seq_str = ''
    with open(reference) as f:
        for line in f:
            if not line.startswith('>'):
                line = line.strip()
                seq_str += line
    return seq_str


def create_windows(seq_str, window):
    l = len(seq_str)
    half_window = window / 2
    chars_list = []
    for index in range(0, l):
        if index >= half_window and index <= (l - half_window):
            chars = seq_str[(index-half_window):(index+half_window+1)]
        # beginning of genome
        elif index < half_window:
            first_pos = (index-half_window)
            second_pos = (index+half_window+1)
            chars = seq_str[first_pos:] + seq_str[0:second_pos]
        # end of genome
        elif index > (l - half_window):
            first_pos = index - half_window
            second_pos = half_window - (l - index)
            chars = seq_str[first_pos:] + seq_str[0:second_pos]
        chars_list.append(chars)
    return chars_list


def GC_content(chars_list):
    file_string = ''
    pos = 0
    for chars in chars_list:
        pos += 1
        GC_count = 0
        for char in chars:
            if char == 'G' or char == 'C':
                GC_count += 1
        GC_fraction = float(GC_count)/len(chars)
        new_line = str(pos) + '\t' + str(GC_fraction) + '\n'
        file_string += new_line
    return file_string


def write_file(name, string):
    with open(name, 'w') as tsv_file:
        tsv_file.write(string)


def main():
    sizes_list = windows.split(',')
    for size in sizes_list:
        window = int(size)
        seq_str = get_sequence(h37rv)
        chars_list = create_windows(seq_str, window)
        file_string = GC_content(chars_list)
        filename = '/grp/valafar/data/depot/blindspots/regions/' + str(window) + 'window_GC_H37Rv.tsv'
        write_file(filename, file_string)


if __name__ == '__main__':
    main()
