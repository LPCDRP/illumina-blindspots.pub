#!/usr/bin/env python3
from ete3 import Tree
import os.path
import shutil
import argparse

parser = argparse.ArgumentParser(description="")
parser.add_argument("-t", "--tree", dest="tree_file", help="Input tree file in newick format", required=True)
args = parser.parse_args()
t = Tree(args.tree_file, format=1)

def search_by_size(node, size):
    node_descendants_dict = {}
    count = 0
    for node in node.traverse():
        if len(node) > size:
            descendant_list = []
            descendants = node.get_descendants()
            for descendant in descendants:
                if descendant.name.startswith('SRR'):
                    descendant_list.append(descendant.name)
            node_descendants_dict[count] = descendant_list
            count += 1
    return node_descendants_dict

node_descendants_dict = search_by_size(t, size=4)

mono_list = []
for line in file('monophyletic_positions.csv'):
    line = line.strip()
    position = line.split(',')[0]
    number_of_isolates = line.split(',')[1:]
    if len(number_of_isolates) > 4:
        mono_list.append(position)


def get_position_dicts():
    mono_position_dict = {}
    para_position_dict = {}
    for line in file('monophyletic_positions.csv'):
        line = line.strip()
        position = line.split(',')[0]
        number_isolates = line.split(',')[1:]
        if len(number_isolates) > 4:
            mono_position_dict[position] = len(number_isolates)
    for line in file('paraphyletic_positions.csv'):
        line = line.strip()
        position = line.split(',')[0]
        number_isolates = line.split(',')[1:]
        para_position_dict[position] = len(number_isolates)
    return mono_position_dict, para_position_dict


def get_isolate_subsets():
    subset_isolate_dict = {}
    for combo_file in os.listdir('/grp/valafar/data/depot/blindspots/comparisons/'):
        isolate_list = []
        if combo_file.endswith('.csv'):
            for line in file('/grp/valafar/data/depot/blindspots/comparisons/' + combo_file):
                if not line.startswith('isolate'):
                    isolate = line.strip().split(',')[0]
                    isolate_list.append(isolate)
            subset_isolate_dict[combo_file] = isolate_list
    return subset_isolate_dict

subset_isolate_dict = get_isolate_subsets()

if os.path.exists('subsets/'):
    shutil.rmtree('subsets/')
os.mkdir('subsets')
for combo,isolates in subset_isolate_dict.items():
    with open('subsets/' + combo, 'wb') as f:
        for line in file('position_isolates.csv'):
            isolate_list = []
            position = line.strip().split(',')[0]
            file_isolates = line.strip().split(',')[1:]
            for isolate in file_isolates:
                if isolate in isolates:
                    isolate_list.append(isolate)
            if len(isolate_list) > 0:
                f.writelines(position)
                f.writelines(',')
                for isolate in isolate_list:
                    f.writelines(isolate)
                    f.writelines(',')
                f.writelines('\n')


for sub_file in os.listdir('subsets/'):
    if sub_file.endswith('.csv'):
        position_list = []
        position_isolate_list_dict = {}
        output_dict = {}
        subset_number = int(sub_file.split('_')[1].split('.')[0])
        for line in file('subsets/' + sub_file):
            line = line.strip()
            position = line.split(',')[0]
            isolates = line.split(',')[1:-1]
            position_list.append(position)
            position_isolate_list_dict[position] = isolates
        for position in position_list:
            mono_poly_list = []
            isos = position_isolate_list_dict[position]
            for node_number, descendants in node_descendants_dict.items():
                if set(descendants).issubset(set(isos)):
                    for descendant in descendants:
                        if descendant not in mono_poly_list:
                            mono_poly_list.append(descendant)
            number_mono_poly_isos = len(mono_poly_list)
            num_isos = len(isos) - number_mono_poly_isos
            print(position, num_isos, (subset_number - number_mono_poly_isos))
            if num_isos > 0:
                if position not in mono_list:
                    output_dict[position] = [num_isos, (subset_number - number_mono_poly_isos)]
        new_subfile_name = sub_file.split('.')[0] + '_comparisons.csv'
        with open('subsets/' + new_subfile_name, 'wb') as f:
            f.writelines('#position,number_of_isolates_with_low_coverage_position,number_of_isolates_with_library_prep_combo')
            f.writelines('\n')
            for position, number_isos_count in sorted(output_dict.iteritems(), key=lambda x: int(x[0])):
                pos_count = number_isos_count[0]
                plat_lib_count = number_isos_count[1]
                f.writelines([str(position), ',', str(pos_count), ',', str(plat_lib_count)])
                f.writelines('\n')

