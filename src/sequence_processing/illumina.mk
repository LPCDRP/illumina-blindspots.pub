.DELETE_ON_ERROR:
INPUTACCESION ?= ~/projects/blindspots/test_Accesion.txt
export datadir ?= $(GROUPHOME)/data/repo/blindspots/
DEST ?= $(GROUPHOME)/data/depot/blindspots/
#ISOLATES := $(notdir $(shell ls $(datadir)/*_1.fastq.gz | cut -f1 -d'_'))
ISOLATES := $(shell cut  -f1 $(INPUTACCESION))

all: $(foreach sample, $(ISOLATES), $(sample).vcf.gz)

%.vcf.gz:
	cd $(DEST) && \
	$(MAKE) -f $(GROUPHOME)/data/depot/blindspots/illumina-single-isolate.mk $@ INSRR=$* && $(RM) *.log *.bam bowtie* trim*


