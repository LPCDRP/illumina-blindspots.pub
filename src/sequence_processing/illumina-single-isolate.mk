.DELETE_ON_ERROR:

REFERENCE := $(GROUPHOME)/resources/H37Rv-NC_000962.3.fasta
NPROC ?= 4
include vcf.mk
include vcf.mk

%_1.fastq.gz %_2.fastq.gz: 
	fastq-dump -I --gzip --split-files $(INSRR)

%_1P.fastq.gz %_2P.fastq.gz: %_1.fastq.gz %_2.fastq.gz
	qsub \
	-cwd \
	-N 'trim$*' \
	-sync yes \
	-q main.q@@valafar \
	-b yes \
	-j yes \
	-pe smp $(NPROC) \
	TrimmomaticPE -threads $(NPROC) $^ -baseout $*.fastq.gz -phred33 \
		      LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 && $(RM) $*_?U.fastq.gz

%.unsorted.sam: %_1P.fastq.gz %_2P.fastq.gz
	qsub \
	-cwd \
	-N 'bowt$*' \
	-q main.q@@valafar \
	-sync yes \
	-b yes \
	-j yes \
	-o bowtie$*.log \
	-pe smp $(NPROC) \
	bowtie2 -p $(NPROC) -x H37Rv -1 $< -2 $(word 2,$^) -S $@

%.bam: %.unsorted.sam
	qsub \
	-cwd \
	-N 'sort$*' \
	-q main.q@@valafar \
	-sync yes \
	-b yes \
	-j yes \
	-o sort$*.log \
	-pe smp $(NPROC) \
	samtools sort -o $@ --threads $(NPROC) --output-fmt BAM $<

%.bam.bai: %.bam
	samtools index $< $@

%.mpileup: %.bam %.bam.bai
	samtools mpileup -q 20 -f $(REFERENCE) $< -o $@

%.LC_positions.txt: %.mpileup
	python LC_positions.py --mpileup $< 
	$(RM) $*_2P.fastq.gz $*_2.fastq.gz

%.vcf: %.mpileup %.LC_positions.txt
	java -jar /home/sbusby/Downloads/varscan.jar mpileup2cns $< --min-avg-qual 20 --min-coverage 10 --variants --output-vcf 1 --strand-filter 0 > $@

.PRECIOUS: %.average_coverage.txt %.LC_positions.txt

