#!/usr/bin/env python3

import argparse
parser = argparse.ArgumentParser(description="""Finds positions in mpileup that have low coverage""")
parser.add_argument("-m", "--mpileup", required = True, help="mpileup file to be analyzed")
args = parser.parse_args()
mpileup = args.mpileup

def main():
    position_dict = create_position_dict(mpileup)
    avg_cov = mean_coverage(position_dict)
    coverage_file = write_mean_coverage(avg_cov)
    positions_file = lc_positions(position_dict,avg_cov)
    return positions_file, coverage_file

def create_position_dict(file):
    """Takes an mpileup file and parses the columns. Creates dictionary with positions as key and
    depth of coverage as value."""
    position_dict = {}
    with open(file,'r') as mpileup:
        for line in mpileup:
            column = line.split()
            name = column[0]
            coordinate = column[1]
            ref_base = column[2]
            reads = column[3]
            position_dict[coordinate] = reads
    return position_dict

def mean_coverage(mpileup_dict):
    """Takes a dictionary with position as key and depth of coverage as value.
        Returns the mean coverage value of specific isolate."""
    # list of coverage values
    k_values = list(mpileup_dict.values())
    k_values = map(float, k_values)
    # mean coverage of specific isolate (i)
    c_iso = sum(k_values) / len(k_values)
    return c_iso

def write_mean_coverage(c_iso):
    """Takes average coverage of isolate and writes it to a new text file."""
    filename = mpileup.split('.')
    SRR = filename[0]
    newfilename = SRR + '.average_coverage.txt'
    with open(newfilename, 'w') as avg_coverage:
        avg_coverage.write(str(c_iso) + '\n')
    return avg_coverage

def lc_positions(mpileup_dict, c_iso):
    """Takes a dictionary with position as key and depth of coverage as value.
        Returns a text file with low coverage positions."""
    # minimum coverage
    d = float(5)
    # mean coverage for a typical sequencing run
    c_ref = float(50)
    # relative coverage
    D = d / c_ref
    # low coverage threshold for i
    d_i = D*c_iso
    #  of low coverage positions in isolate
    klc_dict = {}
    for position in mpileup_dict:
        cov = float(mpileup_dict[position])
        if cov <= d_i:
            klc_dict[position] = int(cov)
    # write each position and coverage to a new line in a text file
    low_cov_string = ''
    for position in klc_dict:
        low_cov_string += str(position) + '\t' + str(klc_dict[position]) + '\n'
    filename = mpileup.split('.')
    SRR = filename[0]
    newfilename = SRR + '.LC_positions.txt'
    with open(newfilename, 'w') as LC_positions:
        LC_positions.write(low_cov_string)
    return LC_positions

if __name__ == '__main__':
    main()


