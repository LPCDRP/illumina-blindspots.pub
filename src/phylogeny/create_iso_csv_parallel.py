#!/usr/bin/env python3
import argparse
import csv
import os
from multiprocessing import Pool
from functools import partial

parser = argparse.ArgumentParser(description="Outputs a csv file that is input for parse_newick.py. This csv is in the "
                                             "format of position,isolate1,isolate2,isolate3....")
parser.add_argument("-i", "--input", dest="variant_directory", help="Enter directory that contains one file per isolate"
                                                                    "that contains one position per line",
                    required=True)
parser.add_argument("--nproc", dest="number_cores", help="Enter the number of cores desired for processing, default"
                   "is 1.", required=False, default=1)
parser.add_argument("-f1", dest="pos_fofn", help='Enter the desired name of the file for all of LC_positions.txt filenames',
                    required=True)
parser.add_argument("-f2", dest="vcf_fofn", help='Enter the desired name of the file for all of vcf.gz filenames',
                    required=True)
parser.add_argument("-o", dest='output_filename', help='Enter desired output csv filename', required=True)
args = parser.parse_args()
variant_dir = args.variant_directory
nproc = args.number_cores
pos_fofn = args.pos_fofn
vcf_fofn = args.vcf_fofn
output_filename = args.output_filename


def check_avg_coverage(coverage_file_path):
    with open(coverage_file_path, 'r') as file:
        for line in file:
            line = line.strip()
            if float(line) > 37:
                return True
            else:
                return False


def get_fofn(blindspot_dir):
    vcf_file_list = []
    position_file_list = []
    coverage_list = []
    for position_file in os.listdir(blindspot_dir):
        if position_file.endswith('.LC_positions.txt'):
            isolate = position_file.split('.')[0]
            if os.path.exists(blindspot_dir + isolate + '.vcf.gz'):
                if check_avg_coverage(blindspot_dir + isolate + '.average_coverage.txt') is True:
                    coverage_list.append('okay')
                    position_file_list.append(blindspot_dir + position_file)
                    vcf_file_list.append(blindspot_dir + isolate + '.vcf.gz')
                else:
                    coverage_list.append('low')
    print(coverage_list.count('okay'))
    print(coverage_list.count('low'))
    return position_file_list, vcf_file_list


def write_files(pos_outfile_name, vcf_outfile_name):
    with open(pos_outfile_name, 'w') as file_handle1:
        for file_path1 in get_fofn(variant_dir)[0]:
            file_handle1.write(file_path1+'\n')
    with open(vcf_outfile_name, 'w') as file_handle2:
        for file_path2 in get_fofn(variant_dir)[1]:
            file_handle2.write(file_path2+'\n')

write_files(pos_fofn, vcf_fofn)


def get_all_positions(variant_fofn):
    all_position_list = []
    with open(variant_fofn, 'r') as var_fofn:
        for variant_file in var_fofn:
            variant_file = variant_file.strip()
            with open(variant_file, 'r') as file:
                for line in file:
                    position = line.strip().split('\t')[0]
                    all_position_list.append(position)
    unique_position_list = set(sorted(all_position_list))
    print(len(unique_position_list))
    return unique_position_list


def get_blindspot_dict(variant_fofn):
    isolate_blindspots_dict = {}
    with open(variant_fofn, 'r') as var_fofn:
        for variant_file in var_fofn:
            isolate = variant_file.split('/')[6].split('.')[0]
            position_list = []
            variant_file = variant_file.strip()
            with open(variant_file, 'r') as file:
                for line in file:
                    position = line.strip().split('\t')[0]
                    position_list.append(position)
                isolate_blindspots_dict[isolate] = position_list
    return isolate_blindspots_dict

iso_bs_dict = get_blindspot_dict(pos_fofn)


def get_output_dict(position):
    print(position)
    output_dict = {}
    isolate_list = []
    for iso, pos_list in iso_bs_dict.items():
        if position in pos_list:
            isolate_list.append(iso)
    output_dict[position] = isolate_list
    return output_dict


def write_files(output_dictionary_list):
    with open(output_filename, 'w') as csvfile:
        writer = csv.writer(csvfile)
        for output_dictionary in sorted(output_dictionary_list):
            for position, isolates in sorted(output_dictionary.items()):
                writer.writerow([position, isolates])


def main():
    load_pool = Pool(processes=int(nproc))
    results = load_pool.map(get_output_dict, get_all_positions(pos_fofn))
    load_pool.close()
    load_pool.join()
    write_files(results)
    cleaned_output_filename = output_filename.replace('.csv', '_cleaned.csv')
    with open(cleaned_output_filename, 'w') as file_guy:
        with open(output_filename, 'r') as file:
            for line in file:
                line = line.replace('[', '').replace(']', '').replace('(', '').replace("'", '').\
                    replace(')', '').replace('"', '').replace(' ', '')
                file_guy.writelines(line)


if __name__ == '__main__':
    main()
