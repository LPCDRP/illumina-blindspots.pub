#!/usr/bin/env python3
import argparse
from Bio import Phylo
from Bio.Phylo import BaseTree
import pandas as pd
from ete3 import Tree
from multiprocessing import Pool
import os
import os.path
import glob

parser = argparse.ArgumentParser(description="Outputs a csv file with the variant of interest, the number of isolates"
                                             "with that variant, and if the isolates make up a monophyetic group on "
                                             "the tree provided.")
parser.add_argument("-i", "--input", dest="blindspot_file", help="Enter a csv file with the variant of "
                                                                                 "interest with the"
                                                              "isolates that have it. Should be in the format of "
                                                              "Variant,Isolate1,isolate2,isolate3...]",required=True)
parser.add_argument("--nproc", dest="number_cores", help="Enter the number of cores desired for processing, default"
                   "is 1.", required=False, default=1)
parser.add_argument("-t", "--tree", dest="tree_file", help="Input tree file in newick format", required=True)
parser.add_argument("-o",dest='output_filename',help='Enter desired output csv filename',required=True)
args = parser.parse_args()

output_filename = args.output_filename
bs_iso_csv = args.blindspot_file
ete3_tree = Tree(args.tree_file)
nproc = args.number_cores
results_list = []
chunk_list = []


lines_per_file = 10000
chunkfile = None
with open(bs_iso_csv) as bigfile:
    for lineno, line in enumerate(bigfile):
        if lineno % lines_per_file == 0:
            if chunkfile:
                chunkfile.close()
            small_filename = '/home/smitchel/blindspots/chunked_input_files/chunked_file_{}.csv'.format(lineno + lines_per_file)
            chunkfile = open(small_filename, "w")
        chunkfile.write(line)
    if chunkfile:
        chunkfile.close()


for chunk_files in os.listdir('/home/smitchel/blindspots/chunked_input_files/'):
    if chunk_files.endswith('.csv'):
        chunk_list.append('/home/smitchel/blindspots/chunked_input_files/' + chunk_files)


def get_variant_dict(variant_file):
    variant_dict = {}
    with open(variant_file, 'r') as file:
        for line in file:
            line = line.strip()
            line = line.replace('[', '').replace(']', '').replace("'", '').replace('"', '')
            variant = line.strip().split(',')[0]
            isolates = line.strip().split(',')[1:]
            variant_dict[variant] = isolates
    return variant_dict


def get_ete3_monophyletic_status(input_list):
    results_dict = {}
    for variant, clades in get_variant_dict(input_list).items():
        print(variant, len(clades))
        isolates_needed_for_monophyly_list = []
        number_isolates_with_variant = len(clades)
        results = ete3_tree.check_monophyly(values=(clades),target_attr='name',ignore_missing=True)
        monophyletic_status = results[1]
        isolates_needed_for_monophyly = results[2]
        for isolate in isolates_needed_for_monophyly:
            isolate = str(isolate).strip().split('--')[1]
            if isolate not in isolates_needed_for_monophyly_list:
                isolates_needed_for_monophyly_list.append(isolate)
        if len(isolates_needed_for_monophyly_list) == 0:
            isolates_needed_for_monophyly_list = 'None'
        if len(isolates_needed_for_monophyly_list) > 30:
            isolates_needed_for_monophyly_list = 'More than Thirty Isolates Needed for Monophyly'
        if number_isolates_with_variant == 1:
            isolates_needed_for_monophyly_list = 'Single Isolate With Blindspot'
        results_dict[variant] = monophyletic_status,number_isolates_with_variant,isolates_needed_for_monophyly_list,\
                                len(isolates_needed_for_monophyly_list)
    return results_dict

def main():
    load_pool = Pool(processes=int(nproc))
    results = load_pool.map(get_ete3_monophyletic_status,chunk_list)
    load_pool.close()
    load_pool.join()
    with open(output_filename, 'w') as f:
        for output_dictionary in sorted(results):
            for position, isolates in output_dictionary.items():
                f.writelines([str(position), str(isolates)])
                f.writelines('\n')


if __name__ == '__main__':
    main()

