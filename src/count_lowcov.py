import os

dir = '/grp/valafar/data/depot/blindspots/cov_files'


def count_lc(file_name):
    count = 0
    with open(file_name, 'r') as lowcov:
        for line in lowcov:
            count += 1
    return count


def write_file(lc_dict, avgcov_dict):
    file_string = 'isolate,lc_positions,avg_coverage\n'
    for iso in lc_dict:
        iso_str = iso + ',' + lc_dict[iso] + ',' + avgcov_dict[iso] + '\n'
        file_string += iso_str
    with open('/grp/valafar/data/depot/blindspots/isolate_lc.csv', 'w') as open_file:
        open_file.write(file_string)


lc_count_dict = {}
average_cov_dict = {}
for file in os.listdir(dir):
    if file.endswith('LC_positions.txt'):
        count = count_lc(dir+'/'+file)
        lc_count_dict[file[:-17]] = str(count)
    if file.endswith('average_coverage.txt'):
        with open(file, 'r') as avgcov:
            for line in avgcov:
                average_cov_dict[file[:-21]] = str(line.strip())
write_file(lc_count_dict, average_cov_dict)