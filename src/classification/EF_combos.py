#!/usr/bin/env python3
import os
import numpy as np

def calculate_Klc(lowcov_file):
    """Calculates KLC for an isolate aka the number of low coverage positions"""
    Klc = 0
    with open('cov_files/'+lowcov_file, 'r') as lowcov_open:
        for line in lowcov_open:
            Klc += 1
    return Klc


def make_del_dict(del_file):
    """Makes dict of isolate and their true del count"""
    del_dict = {}
    with open(del_file, 'r') as counts:
        for line in counts:
            line_list = line.strip().split(',')
            SRR = line_list[0]
            dels = int(line_list[1])
            del_dict[SRR] = dels
    return del_dict


def calculate_fraction(Klc, del_count, ref_size):
    """Calculates the fraction in eq3 given KLC and true del count. The fraction is isolate specific"""
    numerator = Klc-del_count
    denominator = ref_size-del_count
    fraction = numerator/denominator
    return fraction


def calculate_EF(fraction_list, Fp):
    """Finds the median E across isolates in the combination and """
    E = np.median(fraction_list)
    F_list = []
    for fraction in fraction_list:
        F = fraction/(1+(1/Fp))
        F_list.append(F)
    F = np.median(F_list)
    return E, F


def get_EF(true_del_counts, Fp, H37Rv_size, file_list):
    """Returns the E and F values for isolates in a given bootstrap."""
    del_dict = make_del_dict(true_del_counts)
    fraction_list = []
    for file in file_list:
        Klc = calculate_Klc(file)
        SRR = file[0:-17]
        del_count = float(del_dict[SRR])
        fraction = calculate_fraction(Klc, del_count, H37Rv_size)
        fraction_list.append(fraction)
        EF = calculate_EF(fraction_list, Fp)
    return EF



if __name__ == '__main__':
    main()
