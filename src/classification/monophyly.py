def search_by_size(node, size):
    """Traverses a phylogenetic tree and returns a dictionary with node index as key and list
    of descendants as value."""
    node_descendants_dict = {}
    count = 0
    for node in node.traverse():
        if len(node) > size:
            descendant_list = []
            descendants = node.get_descendants()
            for descendant in descendants:
                if descendant.name.startswith('SRR'):
                    descendant_list.append(descendant.name)
            node_descendants_dict[count] = descendant_list
            count += 1
    return node_descendants_dict


def get_mono_list(mono_positions):
    """Takes as input the file with each line containing a monophyletic position and the list of isolates
    that have that position as low coverage. Returns a list of positions in which the number of isolates is greater
    than 4."""
    mono_list = []
    with open(mono_positions, 'r') as file:
        for line in file:
            line = line.strip()
            position = line.split(',')[0]
            number_of_isolates = line.split(',')[1:]
            if len(number_of_isolates) > 4:
                mono_list.append(position)
    return(mono_list)


def get_position_dicts(mono_positions, para_positions):
    """Takes the files with monopyhletic positions and paraphyletic positions with their
    lists of isolates. Returns 2 dictionaries with the positions as keys and list of isos as values."""
    mono_position_dict = {}
    para_position_dict = {}
    with open(mono_positions, 'r') as file:
        for line in file:
            line = line.strip()
            position = line.split(',')[0]
            number_isolates = line.split(',')[1:]
            if len(number_isolates) > 4:
                mono_position_dict[position] = len(number_isolates)
    with open(para_positions, 'r') as file:
        for line in file:
            line = line.strip()
            position = line.split(',')[0]
            number_isolates = line.split(',')[1:]
            para_position_dict[position] = len(number_isolates)
    return mono_position_dict, para_position_dict