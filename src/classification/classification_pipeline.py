#!/usr/bin/env python3
import argparse
from ete3 import Tree
import monophyly
import subset_positions
import EF_combos
import bootstrap_classification
import os
import numpy as np
import shutil
import multiprocessing as mp
from functools import partial
from datetime import datetime
startTime = datetime.now()

parser = argparse.ArgumentParser(description="""Performs all steps of the blind spot classification process.
                                                Randomly samples isolates for a given number of bootstraps (user 
                                                defined values), removes true deletions from phylogenetic filtering,
                                                calculates equations 3-7, and provides list of "blind spots" that
                                                meet thresholds.""")
parser.add_argument("-d", "--directories", required = True, nargs= '+', help="directories containing low coverage "
                                                                             "positions files to pull sample of isolates"
                                                                             " from. These files are output from"
                                                                             "LC_positions.py in the sequence "
                                                                             "processing pipeline")
parser.add_argument("-s", "--sample_size", required = True, help="Number of isolates to select from each directory."
                                                                 "Use the total number of genomes if not performing "
                                                                 "bootstrapping.")
parser.add_argument("-b", "--bootstraps", required = True, help="Number of bootstraps (how many times to randomly"
                                                                " sample). Use value of 1 if not performing "
                                                                "bootstrapping.")
parser.add_argument("-od", "--output_directory", required = True, help="Directory to put all output files in.")
parser.add_argument("-t", "--tree_file", required = False, default =
                    '/home/smitchel/thesis/blindspots_pipeline/blindspots/phylogeny/blindspots.bipartitions',
                    help="blindspots.bipartitions file output from phylogenetic filtering steps. Default is "
                         "/home/smitchel/thesis/blindspots_pipeline/blindspots/phylogeny/blindspots.bipartitions (the "
                         "file used in our analysis).")
parser.add_argument("--nproc", dest="number_cores", help="Enter the number of cores desired for processing, default"
                   "is 1.", required=False, default=1)

args = parser.parse_args()
sources = args.directories
number_isos = int(args.sample_size)
bootstraps = int(args.bootstraps)
output_directory = args.output_directory
tree_file = args.tree_file
nproc = int(args.number_cores)
# variables that don't change
monophyletic_positions = '/home/smitchel/thesis/blindspots_pipeline/monophyletic_positions.csv'
position_isolates = '/home/smitchel/thesis/blindspots_pipeline/position_isolates.csv'
true_del_counts = '/home/smitchel/thesis/blindspots_pipeline/library_combos/true_dels_per_isolate.csv'
t = Tree(tree_file, format=1)
H37Rv_size = float(4411532)
Fp = 0.0000006

def sampling(source):
    """Randomly select a sample of x isolates from a given directory.
    Do this y times (x being number of samples. y being number of bootstraps).
    Output a file with x lists of y SRR's"""
    if os.path.exists(output_directory+source):
        shutil.rmtree(output_directory+source)
    os.system('mkdir ' + output_directory+source)
    random_sampling = 'python /home/crobinho/workspace/blind_spots/illumina-blindspots.pub/src/classification/sample_isos.py -d ' + \
                      source + ' -s ' + str(number_isos) + ' -b ' + str(bootstraps) + ' -o ' + \
                      output_directory+source+'bootstrap_samples.tsv'
    os.system(random_sampling)


def n_G_file(number, bootstrap_file_names, node_descendants_dict, mono_list):
    """Make the dictionary with position | n | G from the list of isolates using their
    LC_positions.txt files. This function will be run with multiprocessing and then written to a csv file."""
    file_list = bootstrap_file_names[number]
    SRR_list = []
    for file in file_list:
        SRR = file[0:-17]
        SRR_list.append(SRR)
    lc_position_isos = subset_positions.pair_lc_pos(position_isolates, SRR_list)
    file_dict = subset_positions.get_n_G(lc_position_isos, node_descendants_dict, mono_list, SRR_list)
    return file_dict


def write_EF_combos(bootstrap_file_names, source):
    """Calclate E and F for the set of isolates using their text files.
    Write them to a file."""
    EF_dict = {}
    EF_file_string = 'combination\tbootstrap\tE\tF\n'
    for bootstrap_number in bootstrap_file_names:
        file_list = bootstrap_file_names[bootstrap_number]
        EF = EF_combos.get_EF(true_del_counts, Fp, H37Rv_size, file_list)
        EF_dict[bootstrap_number] = EF
        EF_file_string += source[:-1] + '\t' + bootstrap_number + '\t' + str(EF[0]) + '\t' + str(EF[1]) + '\n'
    EF_output_name = output_directory+source+source[:-1] + '_EF_combos.tsv'
    with open(EF_output_name, 'w') as output_file:
        output_file.write(EF_file_string)
    return EF_dict


def get_blindspots(EF_dict, source):
    """Classify blind spots using the csv and E&F values.
    Write the blind spots for this bootstrap to a file"""
    output_dir = output_directory + source + 'blindspots/'
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.system('mkdir ' + output_dir)
    for bootstrap in EF_dict:
        E = EF_dict[bootstrap][0]
        F = EF_dict[bootstrap][1]
        ng_dict = bootstrap_classification.get_n_G(output_directory+source+'position_files/bootstrap'+bootstrap+'.csv')
        bs_list = bootstrap_classification.classify(ng_dict, E, F)
        bootstrap_classification.write_file(bs_list, out_name=(
                output_dir+'bs_bootstrap'+bootstrap+'.tsv'))

def main():
    node_descendants_dict = monophyly.search_by_size(t, size=4)
    mono_list = monophyly.get_mono_list(monophyletic_positions)
    now = datetime.now()
    elapsed = now - startTime
    print('After node descendants and mono_list created, time elapsed = ' + str(elapsed))
    # each lib prep/platform combo:
    for source in sources:
        sampling(source)
        bootstrap_file_names = subset_positions.read_samples(output_directory + source + 'bootstrap_samples.tsv')
        pool = mp.Pool(nproc)
        partialfun = partial(n_G_file, bootstrap_file_names=bootstrap_file_names,
                                            node_descendants_dict=node_descendants_dict, mono_list=mono_list)
        results = pool.map(partialfun, bootstrap_file_names)
        pool.close()
        now = datetime.now()
        elapsed = now - startTime
        print('After multiprocessing to get position_n_G dict, time elapsed = ' + str(elapsed))
        bootstrap_count = 0
        output_dir = (output_directory + source + 'position_files/')
        if os.path.exists(output_dir):
            shutil.rmtree(output_dir)
        os.system('mkdir ' + output_dir)
        # each bootstrap:
        for n_G_dict in results:
            bootstrap_count+=1
            subset_positions.write_file(bootstrap_number=bootstrap_count, file_dict= n_G_dict,
                                        output_dir=output_dir)
        EF_dict = write_EF_combos(bootstrap_file_names, source)
        get_blindspots(EF_dict, source)
        print('Combo: ' + source + ' time_elapsed: ' + str(datetime.now() - startTime))


if __name__ == '__main__':
    main()