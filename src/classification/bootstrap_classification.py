import math


def nCk(n,k):
    f = math.factorial
    return f(n) / f(k) / f(n-k)


def get_n_G(bootstrap_filename):
    positions_dict = {}
    with open(bootstrap_filename, 'r') as file:
        for line in file:
            if not line.startswith('#'):
                line = line.strip().split(',')
                position = line[0]
                n = line[1]
                G = line[2]
                positions_dict[position] = (n,G)
    return positions_dict


def classify(n_G_dict, E, F):
    bs_list = []
    for key in n_G_dict:
        position = key
        value = n_G_dict[key]
        n = int(value[0])
        G = int(value[1])
        Epowern = float(E)**n
        p = nCk(G,n)*Epowern
        if p < float(F):
            bs_list.append(int(position))
    bs_list.sort()
    return(bs_list)


def write_file(bs_list, out_name):
    file_string = ''
    for position in bs_list:
        file_string+=str(position)+'\n'
    with open(out_name, 'w') as blindspots:
        blindspots.write(file_string)

