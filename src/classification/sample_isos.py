#!/usr/bin/env python3
import os
from random import sample
import argparse

parser = argparse.ArgumentParser(description="""Randomly selects a sample of isolates from a directory repeatedly.
                                                   Outputs a tsv with file names randomly sampled a given number of times.""")
parser.add_argument("-d", "--directory", required = True, help="directory containing low coverage positions files"
                                                               "to pull sample of isolates from")
parser.add_argument("-s", "--sample_size", required = True, help="Number of isolates to select from the directory")
parser.add_argument("-b", "--bootstraps", required = True, help="Number of bootstraps (how many times to randomly sample")
parser.add_argument("-o", "--output", required = False, default='bootstrap_samples.tsv', help="Output file name")


args = parser.parse_args()
source = os.listdir(args.directory)
number_isos = int(args.sample_size)
bootstraps = int(args.bootstraps)
output = args.output


def bootstrapping(directory, sample_size, bootstraps):
    boot_dict = {}
    file_list = []
    for file in directory:
        if file.endswith('LC_positions.txt'):
            file_list.append(file)
    for i in range(1,bootstraps+1):
        random_isos = sample(file_list, sample_size)
        boot_dict[i] = random_isos
    return(boot_dict)


def write_file(bootstrap_dict, out_name):
    file_string = ''
    for bootstrap in bootstrap_dict:
        file_string += str(bootstrap) + '\t'
        for iso in bootstrap_dict[bootstrap]:
            file_string += iso+'\t'
        file_string += '\n'
    with open(out_name, 'w') as file:
        file.write(file_string)

def main():
    bootstrapping_dict = bootstrapping(source, number_isos, bootstraps)
    write_file(bootstrapping_dict, output)

if __name__ == '__main__':
    main()