import os
import shutil


def read_samples(filename):
    """Reads in the random samples of isolates and outputs a dictionary with bootstrap number
    as key and list of isolate files as value."""
    bootstrap_file_names = {}
    with open(filename, 'r') as bootstrap_samples:
        for line in bootstrap_samples:
            bootstrap_number = line.strip().split()[0]
            isos = line.strip().split()[1:]
            bootstrap_file_names[bootstrap_number] = isos
    return bootstrap_file_names


# each line in the file has position and isolates with that low cov position
def pair_lc_pos(position_isolates, bootstrap_isolates):
    """Takes the file with position and isolates with that position as low coverage and a list of
    isolates that were randomly sampled for a given bootstrap. Returns a dictionary with low coverage
    positions as keys and the isos in this bootstrap sample containing that low cov position as values."""
    lc_position_isos = {}
    with open(position_isolates, 'r') as file:
        for line in file:
            pos_isolate_list = []
            position = line.strip().split(',')[0]
            file_isolates = line.strip().split(',')[1:]
            for isolate in file_isolates:
                if isolate in bootstrap_isolates:
                    pos_isolate_list.append(isolate)
            # pair the position with it's list of isos among this subset that have that position
            if len(pos_isolate_list) > 0:
                lc_position_isos[position] = pos_isolate_list
    return lc_position_isos


def get_n_G(lc_dict, node_descendants_dict, mono_list, bootstrap_isolates):
    """Creates a dictionary with positions as keys (with monopyhletic positions removed) and a list of 2 items
    as values: the number of isolates with that position low coverage and the number of possible isolates
    in which that position could have appeared as low coverage."""
    output_dict = {}
    for position, isos in lc_dict.items():
        mono_poly_list = []
        for node_number, descendants in node_descendants_dict.items():
            if set(descendants).issubset(set(isos)):
                for descendant in descendants:
                    if descendant not in mono_poly_list:
                        mono_poly_list.append(descendant)
        number_mono_poly_isos = len(mono_poly_list)
        num_isos = len(isos) - number_mono_poly_isos
        if num_isos > 0:
            if position not in mono_list:
                output_dict[position] = [num_isos, (len(bootstrap_isolates) - number_mono_poly_isos)]
    return(output_dict)


def write_file(bootstrap_number, file_dict, output_dir):
    """Takes the dictionary with position, n, and G values and writes them to a csv."""
    file_string = '#position,number_of_isolates_with_low_coverage_position,' \
                  'number_of_isolates_with_library_prep_combo' + '\n'
    for position in sorted(file_dict, key=lambda x: int(x[0])):
        number_isos_count = file_dict[position]
        pos_count = number_isos_count[0]
        plat_lib_count = number_isos_count[1]
        file_string += str(position) + ',' + str(pos_count) + ',' + str(plat_lib_count) + '\n'
    new_subfile_name = 'bootstrap' + str(bootstrap_number) + '.csv'
    with open(output_dir + new_subfile_name, 'w') as f:
        f.write(file_string)
